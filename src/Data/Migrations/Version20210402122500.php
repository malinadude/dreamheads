<?php

declare(strict_types=1);

namespace App\Data\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210402122500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE language 
                ADD COLUMN test VARCHAR(255),
                DROP COLUMN id,
                ADD COLUMN id INT PRIMARY KEY AUTO_INCREMENT
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('
            ALTER TABLE language 
                DROP COLUMN test,
                MODIFY COLUMN id INT PRIMARY KEY
        ');
    }
}
