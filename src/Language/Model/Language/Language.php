<?php
declare(strict_types=1);

namespace App\Language\Model\Language;

use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @ORM\Table(name="language")
 * @ORM\Entity()
 */
class Language
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="language_language_name")
     */
    private Name $name;

    /**
     * @ORM\Column(type="language_language_test")
     */
    private Test $test;

    /**
     * Language constructor.
     * @param Name $name
     * @param Test $test
     */
    public function __construct(Name $name, Test $test)
    {
        $this->name = $name;
        $this->test = $test;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }

    public function update(?Name $name, ?Test $test)
    {
        if ($name) {
            $this->name = $name;
        }

        if ($test) {
            $this->test = $test;
        }
    }
}
