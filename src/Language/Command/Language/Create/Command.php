<?php

declare(strict_types=1);

namespace App\Language\Command\Language\Create;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Command.
 */
class Command
{
    /**
     * @Assert\NotBlank()
     */
    private string $name;
    private string $test;

    /**
     * Command constructor.
     * @param string $name
     * @param string $test
     */
    public function __construct(string $name, string $test)
    {
        $this->name = $name;
        $this->test = $test;
    }

    /**
     * @return string
     */
    public function getTest(): string
    {
        return $this->test;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}
