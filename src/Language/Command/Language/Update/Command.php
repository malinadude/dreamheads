<?php

declare(strict_types=1);

namespace App\Language\Command\Language\Update;

/**
 * Command.
 */
class Command
{
    private int $id;
    private string $name;
    private string $test;

    /**
     * Command constructor.
     * @param int $id
     * @param string $name
     * @param string $test
     */
    public function __construct(int $id, string $name, string $test)
    {
        $this->id = $id;
        $this->name = $name;
        $this->test = $test;
    }

    /**
     * @return string
     */
    public function getTest(): string
    {
        return $this->test;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}
